
function CreatePseudoRandomObject()
{
	return {
				StartTimeStamp:+ new Date(),
				LastTimeStamp:+ new Date(),
				RandomFloat:0,
				RandomFloatMax:1000,
				RandomFloatMin:0,
				MinInterval:200,
				MaxInterval:400,
				AsyncThreadsIDs:[],
				GeneratedNumbers:new Array(),
				GenNumMax:30000,
				GetRandomIntInsecure:function(From,To)
				{
					try
					{
						return Math.floor(Math.random()*(To-From+1)+From);
					}
					catch(err)
					{
						throw{name:"GetRandomIntInsecure error",message:"GetRandomIntInsecure: "+err.message}
					}
				},
				GenRandomFloat:function(DoNotStore)
				{
					try
					{
						var CurrTimeStamp = + new Date();
						var Diff = (this.LastTimeStamp - CurrTimeStamp)+(CurrTimeStamp-this.StartTimeStamp),
							Abs = Math.abs(this.RandomFloatMin - this.RandomFloatMax),
							Offset = (Math.abs(this.RandomFloat)/2)+this.RandomFloat;
						
						this.RandomFloat = ((Math.abs(Diff)+Offset) % Abs)+this.RandomFloatMin;
						this.LastTimeStamp = + new Date();

						if(!DoNotStore)
						{
							if(this.GeneratedNumbers.length < this.GenNumMax)
							{
								if(this.GeneratedNumbers[this.GeneratedNumbers.length-1] != this.RandomFloat)
								{
									this.GeneratedNumbers.push(this.RandomFloat);
								}
							}
							else
							{
								this.GeneratedNumbers.shift();
							}
						}
						
						return this.RandomFloat;
					}
					catch(err)
					{
						throw{name:"GenRandomFloat error",message:"GenRandomFloat: "+err.message}
					}
				},
				GetGeneratedNumber:function()
				{
					try
					{
						if(this.GeneratedNumbers.length > 0)
						{
							return this.GeneratedNumbers.shift();
						}
						else
						{
							return this.GenRandomFloat(true);
						}
					}
					catch(err)
					{
						throw{name:"GetGeneratedNumber error",message:"GetGeneratedNumber: "+err.message}
					}
				},
				GetGeneratedNumberAsInt:function()
				{
					try
					{
						return Math.floor(this.GetGeneratedNumber());
					}
					catch(err)
					{
						throw{name:"GetGeneratedNumberAsInt error",message:"GetGeneratedNumberAsInt: "+err.message}
					}
				},
				GetGeneratedNumberAsIntBetweenRanges:function(From,To)
				{
					try
					{
						return Math.floor(this.GetGeneratedNumber()*(To-From+1)+From)
					}
					catch(err)
					{
						throw{name:"GetGeneratedNumberAsIntBetweenRanges error",message:"GetGeneratedNumberAsIntBetweenRanges: "+err.message}
					}
				},
				SetRandomFloat:function()
				{
					try
					{
						this.RandomFloat = this.GenRandomFloat();
					}
					catch(err)
					{
						throw{name:"SetRandomFloat error",message:"SetRandomFloat: "+err.message}
					}
				},
				Initialise:function()
				{
					try
					{
						this.SetRandomFloat();
					}
					catch(err)
					{
						throw{name:"Initialise error",message:"Initialise: "+err.message}
					}
				},
				GenerateRandomInterval:function(Min,Max)
				{
					try
					{
						return Math.round((Math.abs(this.GenRandomFloat(+ new Date())) % Max)+Min);
					}
					catch(err)
					{
						throw{name:"GenerateRandomInterval error",message:"GenerateRandomInterval: "+err.message}
					}
				},
				UpdateRandomFloat:function(DebugMode)
				{
					try
					{
						this.RandomFloat = this.GenRandomFloat();
					}
					catch(err)
					{
						throw{name:"UpdateRandomFloat error",message:"UpdateRandomFloat: "+err.message}
					}
				},
				AddThread:function(DebugMode)
				{
					try
					{
						var Temp = Math.round(this.GenerateRandomInterval(this.MinInterval,this.MaxInterval));
						
						this.AsyncThreadsIDs.push(setInterval(this.UpdateRandomFloat.bind(this,DebugMode),Temp));
					}
					catch(err)
					{
						throw{name:"AddThread error",message:"AddThread: "+err.message}
					}
				},
				AddThreads:function(Amount)
				{
					try
					{
						for(var Iter = 0;Iter < Amount;Iter++)
						{
							this.AddThread((Amount > 800));
						}
					}
					catch(err)
					{
						throw{name:"AddThreads error",message:"AddThreads: "+err.message}
					}
				},
				RemoveThread:function()
				{
					try
					{
						if(this.AsyncThreadsIDs.length > 0)
						{
							clearInterval(this.AsyncThreadsIDs.shift());
							return true;
						}
						return false;
					}
					catch(err)
					{
						throw{name:"RemoveThread error",message:"RemoveThread: "+err.message}
					}
				},
				ClearThreads:function()
				{
					try
					{
						while(this.RemoveThread());
					}
					catch(err)
					{
						throw{name:"ClearThreads error",message:"ClearThreads: "+err.message}
					}
				}
			};
}

function CreateAdvancedPseudoRandomObject(TMinRandom,TMaxRandom,TMinInterval,TMaxInterval,TNumberOfThreads,TMaximumNumberOfNumbersToStore)
{
	var Temp = {
					CryptoPRNG:null,
					ConstructCryptoPRNG:function(MinRandom,MaxRandom,MinInterval,MaxInterval,NumberOfThreads,MaximumNumberOfNumbersToStore)
					{
						try
						{
							var PRO = CreatePseudoRandomObject();
							PRO.RandomFloatMin = (IsNumber(MinRandom)) ? MinRandom : 0;
							PRO.RandomFloatMax = (IsNumber(MaxRandom)) ? MaxRandom : 3333;
							PRO.MinInterval = (IsNumber(MinInterval)) ? MinInterval : 1;
							PRO.MaxInterval = (IsNumber(MaxInterval)) ? MaxInterval : 40;
							PRO.GenNumMax = (IsNumber(MaximumNumberOfNumbersToStore)) ? MaximumNumberOfNumbersToStore : 30000;
							PRO.Initialise();
							
							var TempNumberOfThreads = (IsNumber(NumberOfThreads)) ? NumberOfThreads : 40;
							
							PRO.AddThreads(TempNumberOfThreads);
							return PRO;
						}
						catch(err)
						{
							throw{name:"ConstructCryptoPRNG error",message:"ConstructCryptoPRNG: "+err.message}
						}
					},
					Initialise:function(MinRandom,MaxRandom,MinInterval,MaxInterval,NumberOfThreads,MaximumNumberOfNumbersToStore)
					{
						try
						{
							this.CryptoPRNG = this.ConstructCryptoPRNG(MinRandom,MaxRandom,MinInterval,MaxInterval,NumberOfThreads,MaximumNumberOfNumbersToStore);//(0,3333,1,40,40,30000);
						}
						catch(err)
						{
							throw{name:"Initialise error",message:"Initialise: "+err.message}
						}
					},
					GenerateRandomArray:function(PermittedArrayOfItems,LengthOfArray)
					{
						try
						{
							var Collector = [];
							var Rando = 0;
							
							for(var Iter = 0;Iter < LengthOfArray;Iter++)
							{
								Rando = Math.round(this.CryptoPRNG.GetGeneratedNumber());
								Collector.push(PermittedArrayOfItems[Rando]);
							}
							return Collector;
						}
						catch(err)
						{
							throw{name:"GenerateRandomArray error",message:"GenerateRandomArray: "+err.message}
						}
					},
					GenerateRandomUint8Array:function(LengthOfArray)
					{
						try
						{
							var Collector = new Uint8Array(LengthOfArray);
							
							for(var Iter = 0;Iter < LengthOfArray;++Iter)
							{
								Collector[Iter] = Math.round(this.CryptoPRNG.GetGeneratedNumber()) % 255;
							}
							
							return Collector;
						}
						catch(err)
						{
							throw{name:"GenerateRandomUint8Array error",message:"GenerateRandomUint8Array: "+err.message}
						}
					}
				};
				
	Temp.Initialise(TMinRandom,TMaxRandom,TMinInterval,TMaxInterval,TNumberOfThreads,TMaximumNumberOfNumbersToStore);
	return Temp;
}
